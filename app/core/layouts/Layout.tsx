import Head from "next/head"
import React, { FC } from "react"
import { BlitzLayout } from "@blitzjs/next"
import Navegador from "../components/Navegador";

const Layout: BlitzLayout<{ title?: string; children?: React.ReactNode }> = ({
  title,
  children,
}) => {
  return (
    <>
      <Head>
        <title>{title || "app-onload"}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navegador></Navegador>

      <div className="">
      {children}
      </div>
    </>
  )
}

export default Layout
