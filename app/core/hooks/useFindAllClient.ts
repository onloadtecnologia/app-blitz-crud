import { useQuery } from "@blitzjs/rpc"
import findAllClient from "app/cliente/queries/findAllClient"

export const useFindAllClient = () => {
  const [clientes] = useQuery(findAllClient, null)
  return clientes;
}
