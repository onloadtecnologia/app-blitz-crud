import { Routes } from "@blitzjs/next"
import Link from "next/link"

export default function Navegador() {
  return (
    <nav>
      <ul>
        <li>
          <Link href={Routes.Home()}>
            <a>Home</a>
          </Link>
        </li>
        <li>
          <Link href={Routes.Cadastro()}>
            <a>Cadastro</a>
          </Link>
        </li>
        <li>
          <Link href={Routes.Clientes()}>
            <a>Clientes</a>
          </Link>
        </li>
        <li>
          <Link href={Routes.LoginPage()}>
            <a>Login</a>
          </Link>
        </li>
        <li>
          <Link href={Routes.SignupPage()}>
            <a>Signup</a>
          </Link>
        </li>
      </ul>
    </nav>
  )
}
