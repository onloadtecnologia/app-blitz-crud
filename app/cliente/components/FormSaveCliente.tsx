import { useMutation } from "@blitzjs/rpc"
import SaveClient from "app/cliente/mutations/saveClient"
import { useEffect, useState } from "react"

export default function FormSaveClient() {
  const [saveCliente] = useMutation(SaveClient)

  const [cliente, setCliente] = useState({
    name: "",
    telefone: "",
    email: "",
    cep: "",
  })

  useEffect(() => {})

  const save = async (e) => {
    e.preventDefault()
    await saveCliente(cliente)
  }

  return (
    <>
      <form action="" method="post" onSubmit={save}>
        <div className="mb-3">
          <label htmlFor="name">Nome</label>
          <input
            type="text"
            onChange={(name) => setCliente({ ...cliente, name: name.target.value })}
            name="name"
            id="name"
          />
        </div>
        <div className="mb-3">
          <label htmlFor="telefone">Telefone</label>
          <input
            type="tel"
            onChange={(telefone) => setCliente({ ...cliente, telefone: telefone.target.value })}
            name="telefone"
            id="telefone"
          />
        </div>
        <div className="mb-3">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            onChange={(email) => setCliente({ ...cliente, email: email.target.value })}
            name="email"
            id="email"
          />
        </div>
        <div className="mb-3">
          <label htmlFor="cep">Cep</label>
          <input
            type="text"
            onChange={(cep) => setCliente({ ...cliente, cep: cep.target.value })}
            name="cep"
            id="cep"
          />
        </div>
        <input type="submit" value="save" />
      </form>
    </>
  )
}
