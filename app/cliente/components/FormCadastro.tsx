import { useMutation } from "@blitzjs/rpc"
import { Field, Form } from "react-final-form"
import SaveClient from "../mutations/saveClient"

const FormCadastro = () => {
  const [saveClient] = useMutation(SaveClient)

  const onSubmit = async (values) => {
    window.alert(JSON.stringify(values))
    await saveClient(values)
  }

  const validate = (values) => {
    const error: any = {}
    if (!values.name) {
      error.name = "Required"
    }
    if (!values.telefone) {
      error.telefone = "Required"
    }
    if (!values.email) {
      error.email = "Required"
    }
    if (!values.cep) {
      error.cep = "Required"
    }
    return error
  }
  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      initialValues={{ name: "", telefone: "", email: "", cep: "" }}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Field name="name">
            {({ input, meta }) => (
              <div className="mb-3">
                <label htmlFor="name">Name</label>
                <input {...input} name="name" type="text" placeholder="name" />
                {meta.touched && meta.error && <span>{meta.error}</span>}
              </div>
            )}
          </Field>

          <Field name="telefone">
            {({ input, meta }) => (
              <div className="mb-3">
                <label htmlFor="telefone">Telefone</label>
                <input {...input} name="telefone" type="phone" placeholder="telefone" />
                {meta.touched && meta.error && <span>{meta.error}</span>}
              </div>
            )}
          </Field>

          <Field name="email">
            {({ input, meta }) => (
              <div className="mb-3">
                <label htmlFor="email">Email</label>
                <input {...input} name="email" type="email" placeholder="email" />
                {meta.touched && meta.error && <span>{meta.error}</span>}
              </div>
            )}
          </Field>

          <Field name="cep">
            {({ input, meta }) => (
              <div className="mb-3">
                <label htmlFor="cep">Cep</label>
                <input {...input} name="cep" type="text" placeholder="cep" />
                {meta.touched && meta.error && <span>{meta.error}</span>}
              </div>
            )}
          </Field>

          <div className="mb-3">
            <input type="submit" value="salvar" />
          </div>
        </form>
      )}
    />
  )
}

export default FormCadastro
