import { Cliente } from "@prisma/client"
import { useFindAllClient } from "app/core/hooks/useFindAllClient"
const Tabela = () => {
  let clientes: Array<Cliente> = useFindAllClient()

  return (
    <>
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Data</th>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Email</th>
            <th>Cep</th>
          </tr>
        </thead>
        <tbody>
          {clientes.map((cl) => (
            <tr key={cl.id}>
              <td>{cl.id}</td>
              <td>{cl.data.toLocaleDateString("pt-br")}</td>
              <td>{cl.name}</td>
              <td>{cl.telefone}</td>
              <td>{cl.email}</td>
              <td>{cl.cep}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default Tabela
