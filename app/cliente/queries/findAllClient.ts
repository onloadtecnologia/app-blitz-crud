import db from 'db'


export default async function findAllClient(_ = null) {
   const clientes = await db.cliente.findMany();
   return clientes;
}
