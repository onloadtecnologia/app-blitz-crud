import db, { Cliente } from "db"

export default async function SaveClient(cliente) {
  try {
    let res = await db.cliente.create({
      data: {
        id: undefined,
        data: undefined,
        name: cliente.name,
        telefone: cliente.telefone,
        email: cliente.email,
        cep: cliente.cep,
      },
      select: { id: true, data: true, name: true, telefone: true, email: true, cep: true },
    })
    return res
  } catch (error) {
    console.error(error.message)
  }
}
