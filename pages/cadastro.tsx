import FormCadastro from "app/cliente/components/FormCadastro"
import Layout from "app/core/layouts/Layout"

export default function Cadastro() {
  return (
    <Layout title="Cadastro">
        <FormCadastro></FormCadastro>
    </Layout>
  )
}
