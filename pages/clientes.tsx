import Tabela from "app/cliente/components/Tabela"
import Layout from "app/core/layouts/Layout"
import { Suspense, useEffect } from "react"

const Clientes = () => {

  useEffect(() => {
  })

  return (
    <Layout title="Clientes">
      <Suspense fallback="Loading...">
        <Tabela></Tabela>
      </Suspense>
    </Layout>
  )
}

export default Clientes
